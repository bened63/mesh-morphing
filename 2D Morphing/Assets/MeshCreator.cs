﻿using System.Collections;

#if UNITY_EDITOR

using UnityEditor;

#endif

using UnityEngine;

public class MeshCreator : MonoBehaviour {

    public enum Shape {
        Quad, Circle
    }

    [SerializeField] private int _width = 1;
    [SerializeField] private int _height = 1;
    [SerializeField] private int _morphDuration = 1;
    [SerializeField] private bool _showGizmos = false;
    [SerializeField] private Shape _shape = Shape.Quad;
    [Header("DO NOT CHANGE")]
    [SerializeField] private string _shapeStatus = Shape.Quad.ToString();

    private Vector3[] _vertices;
    private Vector3[] _quadVertices;
    private Vector3[] _circleVertices;
    private int[] _tris;
    private Vector3 _center;

    private Mesh _mesh;
    private MeshRenderer _meshRenderer;
    private MeshFilter _meshFilter;

    public Shape shape { get => _shape; set => _shape = value; }

    private void Start() {
        _mesh = new Mesh();
        _meshFilter = gameObject.GetComponent<MeshFilter>();
        _meshFilter.mesh = _mesh;

        _meshRenderer = gameObject.GetComponent<MeshRenderer>();
        _meshRenderer.sharedMaterial = new Material(Shader.Find("Standard"));

        CreateShape();
        UpdateMesh();
        //StartCoroutine(IEChangeShape());
    }

    private void CreateShape() {
        int vertexCount = 0; // Vertex count without center
        _center = new Vector3(0.5f * _width, 0.5f * _height);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Quad vertices (workaround!)

        // Compute the numer of vertices
        // One vertex in the center of the mesh
        // All other vertices are on the border of the mesh
        _quadVertices = new Vector3[2 * (_width + 1) + 2 * (_height - 1) + 1];
        _quadVertices[_quadVertices.Length - 1] = _center;

        // Bottom vertices (left to right)
        for (int x = 0; x <= _width; x++) {
            _quadVertices[vertexCount] = new Vector3(x, 0, 0);
            vertexCount++;
        }

        // Vertices on the right side (bottom to top)
        // Ignore first vertex in the corner because it was already added before
        for (int y = 1; y <= _height; y++) {
            _quadVertices[vertexCount] = new Vector3(_width, y, 0);
            vertexCount++;
        }

        // Vertices on the top (now right to left!)
        // Ignore first vertex in the corner because it was already added before
        for (int x = _width - 1; x >= 0; x--) {
            _quadVertices[vertexCount] = new Vector3(x, _height, 0);
            vertexCount++;
        }

        // Vertices on the left (top to bottom)
        // Ignore first and last vertex in the corners because it was already added before
        for (int y = _height - 1; y > 0; y--) {
            _quadVertices[vertexCount] = new Vector3(0, y, 0);
            vertexCount++;
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Circle vertices (workaround!)

        _circleVertices = new Vector3[_quadVertices.Length];
        float centerX = _center.x;
        float centerY = _center.y;
        float centerZ = _center.z;
        float radius = 0.3f * _width;

        for (int i = 0; i < vertexCount; i++) {
            float x = centerX + (float)Mathf.Cos(2 * Mathf.PI * i / vertexCount - (3 * Mathf.PI * 0.25f)) * radius;
            float y = centerY + (float)Mathf.Sin(2 * Mathf.PI * i / vertexCount - (3 * Mathf.PI * 0.25f)) * radius;
            float z = centerZ;
            _circleVertices[i] = new Vector3(x, y, z);
        }
        _circleVertices[_circleVertices.Length - 1] = _center;

        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Triangles

        _tris = new int[3 * vertexCount];
        int tris = 0;
        for (int i = 0; i < _quadVertices.Length - 1; i++) {
            _tris[tris] = i;
            _tris[tris + 1] = _quadVertices.Length - 1;
            _tris[tris + 2] = i + 1 >= vertexCount ? 0 : i + 1;
            tris += 3;
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Start shape

        switch (_shape) {
            case Shape.Quad:
                _vertices = (Vector3[])_quadVertices.Clone();
                break;

            case Shape.Circle:
                _vertices = (Vector3[])_circleVertices.Clone();
                break;

            default:
                break;
        }
    }

    private void UpdateMesh() {
        _mesh.Clear();
        _mesh.vertices = _vertices;
        _mesh.triangles = _tris;
        _mesh.RecalculateNormals();
    }

    private IEnumerator IEChangeShape() {
        float lerpTime = _morphDuration;
        float elapsedTime = 0;
        bool running = true;
        Vector3[] target = new Vector3[0];
        _shapeStatus = "...morphing...";

        yield return new WaitForEndOfFrame();

        while (running) {
            //if (!_enableMorphing) {
            //    elapsedTime = 0;
            //    yield return null;
            //    continue;
            //}

            if (_shape == Shape.Circle) {
                target = _circleVertices;
            }
            else if (_shape == Shape.Quad) {
                target = _quadVertices;
            }

            elapsedTime += Time.deltaTime;
            if (elapsedTime > lerpTime) elapsedTime = lerpTime;

            float t = elapsedTime / lerpTime;
            t = t * t * t * (t * (6f * t - 15f) + 10f);

            for (int i = 0; i < _vertices.Length - 1; i++) {
                _vertices[i] = Vector3.Lerp(_vertices[i], target[i], t);
            }
            UpdateMesh();

            // Finished
            if (elapsedTime == lerpTime) {
                running = false;
                _shapeStatus = _shape.ToString();
            }
            yield return null;
        }
    }

    private float AngleBetweenVector2(Vector2 vec1, Vector2 vec2) {
        Vector2 vec1Rotated90 = new Vector2(-vec1.y, vec1.x);
        float sign = (Vector2.Dot(vec1Rotated90, vec2) < 0) ? -1.0f : 1.0f;
        return Vector2.Angle(vec1, vec2) * sign;
    }

    private void OnValidate() {
        if (Application.isPlaying) {
            if (_shapeStatus != _shape.ToString()) {
                StopAllCoroutines();
                StartCoroutine(IEChangeShape());
            }
        }
    }

    #region Gizmos

    private void OnDrawGizmos() {
        if (!_showGizmos) return;

        // Quad
        if (_quadVertices == null) return;
        int counter = 0;
        foreach (var v in _quadVertices) {
#if UNITY_EDITOR
            Handles.Label(v, counter.ToString());
            counter++;
#endif
            Gizmos.DrawSphere(v, 0.035f);
        }

        // Circle
        if (_circleVertices == null) return;
        Gizmos.color = Color.blue;
        counter = 0;
        foreach (var v in _circleVertices) {
#if UNITY_EDITOR
            Handles.Label(v, counter.ToString());
            counter++;
#endif
            Gizmos.DrawSphere(v, 0.035f);
        }
    }

    #endregion Gizmos
}